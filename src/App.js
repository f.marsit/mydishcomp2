import React from 'react';
import Comb from './component/comb';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

function App() {

  return (
    <Router>

      <div className="background">
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/comb">ComB 1</Link>
          </li>

        </ul>
        <div className="container">
          <Switch>
          
            <Route path="/comb" component={Comb}></Route>
            <Route path="/">
              <Home />
            </Route>
          </Switch>


        </div>
      </div>

    </Router>

  );
}

function Home() {
  return <h2>Home</h2>;
}
export default App;
