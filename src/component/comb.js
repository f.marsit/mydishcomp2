import React from "react"
import InputText from "./inputText"
import Button from "./button"
import data from "./data.json"

class Comb extends React.Component {
    constructor() {
        super();
        this.state = {
            data: data.comb,
            inputValue: '',
            disabled: true
        }
        this.handleChange = this.handleChange.bind(this)
    }

    handleChange = (e) => {
        this.setState({
            inputValue: e.target.value,
            disabled: e.target.value ? false : true

        })
        console.log(e.target.value)

    }

    render() {
        return (
            <div>
                <h1>{this.state.data.message}</h1>
                <span>{this.state.data.text}</span>
                <InputText placeholder={this.state.data.input.placeholder}
                    handleChange={this.handleChange}

                ></InputText>
                <Button text={this.state.data.button.text} disabled={this.state.disabled}  ></Button>


            </div>
        )

    }

}
export default Comb;