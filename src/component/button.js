import React from "react"

function Button(props) {
    return(
    <button className={props.disabled ? "button" :"buttonActive" }
    disabled={props.disabled}> 
        <span className="buttonText">{props.text}</span>
    </button>
    )

}

export default Button