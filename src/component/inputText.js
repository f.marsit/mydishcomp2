import React from "react";

function InputText(props) {
    return (
        <div className="float-container">

            <input type={props.type} 
            placeholder={props.placeholder} 
           
            onChange={ props.handleChange} />

        </div>


    )
}
export default InputText;